package com.test.java.gpio;

import com.pi4j.io.gpio.*;
import com.pi4j.platform.Platform;
import com.pi4j.platform.PlatformAlreadyAssignedException;
import com.pi4j.platform.PlatformManager;

public class Gpio {
    private  GpioController gpio ;
    public Gpio() {

        try {
            PlatformManager.setPlatform(Platform.ORANGEPI);
        } catch (PlatformAlreadyAssignedException e) {
            e.printStackTrace();
        }
        gpio= GpioFactory.getInstance();
    }
    public void blink(){
        GpioPinDigitalOutput myLed = gpio.provisionDigitalOutputPin(OrangePiPin.GPIO_08);
        for (int i =0;i<10;i++) {
            try {
                myLed.high();
                Thread.sleep(500);
                myLed.low();
                Thread.sleep(500);
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
